package me.aemc.dmo.hello_world;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.net.InetAddress;
import java.net.UnknownHostException;

class InfoData {
  String hostname  = "unknown";
  String ipAddress = "unknown";
  public String getHostname(){ return this.hostname; }
  public String getIpAddress(){ return this.ipAddress; }
}

@Path("/info-host")
public class InfoController
{
  @GET
  @Produces( MediaType.APPLICATION_JSON)
  public InfoData get(){
    InfoData data = new InfoData();
    
    try {
      InetAddress ip = InetAddress.getLocalHost();
      data.hostname  = ip.getHostName();
      data.ipAddress = ip.getHostAddress();
    } catch( UnknownHostException e ) {
      e.printStackTrace();
    }
    
    return data;
  }
}
