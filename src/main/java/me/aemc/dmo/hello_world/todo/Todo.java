package me.aemc.dmo.hello_world.todo;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;

@Entity( name = "todo" )
public class Todo extends PanacheEntity
{
	public String title;
	public String content;
}