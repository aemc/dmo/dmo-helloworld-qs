package me.aemc.dmo.hello_world.todo;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Path("/todos")
public class TodoController {

  @Inject TodoRepository todoRepository;

  @PostConstruct
  void init(){
    this.todoRepository.init();
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Set<Todo> handleGet() {
    List<Todo> list = this.todoRepository.findAll().list();
    return new HashSet<>( list );
  }
}
