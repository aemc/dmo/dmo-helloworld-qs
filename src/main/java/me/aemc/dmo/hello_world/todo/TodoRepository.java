package me.aemc.dmo.hello_world.todo;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class TodoRepository implements PanacheRepositoryBase<Todo, Integer>
{
  @Transactional
  public void init(){
    List<Todo> todos = Todo.listAll();
    if( todos.isEmpty() ){
      Todo todo = new Todo();
      todo.title = "Prepare talk for JSD2019";
      todo.persist();
    }
  }
}
