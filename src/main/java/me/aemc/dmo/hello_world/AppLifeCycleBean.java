package me.aemc.dmo.hello_world;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import io.quarkus.runtime.configuration.ProfileManager;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

@ApplicationScoped
public class AppLifeCycleBean
{
  final Logger log = LoggerFactory.getLogger( this.getClass().getName() );
  
  @ConfigProperty( name= "quarkus.datasource.db-kind"  ) String dsDbKind;
  @ConfigProperty( name= "quarkus.datasource.jdbc.url" ) String dsJdbcUrl;
  
  void onStart( @Observes StartupEvent event ){
  
    String profile = ProfileManager.getActiveProfile();
    
    this.log.info( "Starting ..." );
    this.log.info( ".. quarkus.profile            : " + profile );
    this.log.info( ".. quarkus.datasource.db-kind : " + this.dsDbKind );
    this.log.info( ".. quarkus.datasource.jdbc.url: " + this.dsJdbcUrl );
  }
  
  void onStop( @Observes ShutdownEvent event ){
    this.log.info( "Stopping ..." );
  }
}
